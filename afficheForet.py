def drawLines(canvas, w, h, s):
    s += 1
    totalHeight = s * h + 2
    totalWidth = s * w + 2
    for i in range(w+1):
        canvas.create_line(s*i+1, 1, s*i+1, totalHeight, fill="#AAA")
    for i in range(h+1):
        canvas.create_line(1, s*i+1, totalWidth, s*i+1, fill="#AAA")

def drawForet(canvas, w, h, s, foret):
    s += 1
    for i in range(w):
        for j in range(h):
            if foret[i][j] == 0:
                color = "darkgreen"
            elif foret[i][j] == 1:
                color = "red"
            elif foret[i][j] == 2:
                color = "brown"
            else:
                color = "grey"
            canvas.create_rectangle(s*i+1, s*j+1, s*(i+1)+1, s*(j+1)+1, fill=color)