# coding: utf-8
import argparse
import os
from tkinter import *
import random
import datetime
from bruleforetbrule import *
from afficheForet import *

parser = argparse.ArgumentParser(description="Simulateur de feu de forêt basé sur un automate cellulaire.")
parser.add_argument("-l", "--largeur", help="Largeur de la forêt (par défaut 10)", type=int, default=10)
parser.add_argument("-H", "--hauteur", help="Hauteur de la forêt (par défaut 10)", type=int, default=10)
parser.add_argument("-c", "--cellules", help="Taille d'une cellule en pixels (par défaut 20)", type=int, default=20)
parser.add_argument("-t", "--trees", help="Probabilité d'avoir un arbre dans une cellule (en pourcent, par défaut 70)", default=70, type=int)
parser.add_argument("-s", "--speed", help="Vitesse de l'animation (en secondes, par défaut 2, minimum=0.1)", default=2, type=float)
parser.add_argument("-v", "--voisinage", help="Quand cette option est renseignée, le voisinage de Moore (8) est utilisé au lieu de celui de Von Neumann (4).", action="store_true")
args = parser.parse_args()
width = args.largeur
height = args.hauteur
size = args.cellules
trees = args.trees
voisinageMoore = args.voisinage
seconds = args.speed

simulation = False

def stepbystep():
    continueDeBruler(foret, voisinageMoore)
    drawForet(canvas, width, height, size, foret)

def playstop():
    global simulation
    if simulation:
        simulation = False
    else:
        simulation = True

def click_callback(event):
    x = event.x // (size + 1)
    y = event.y // (size + 1)
    if event.num == 1:
        setFire(x, y, foret)
        drawForet(canvas, width, height, size, foret)

main = Tk()
main.columnconfigure(3)
main.rowconfigure(3)
main.title("Simulateur de feu de forêt")

boutonStep = Button(main, text="Etape suivante", command=stepbystep)
boutonStep.grid(row=2, column=2)

boutonPlay = Button(main, text="Lecture / Pause", command=playstop)
boutonPlay.grid(row=2, column=1)

canvas = Canvas(main, width=((size + 1) * width + 1), height=((size + 1) * height + 1))
canvas.grid(column=0, columnspan=3, row=1)
canvas.bind('<Button-1>', click_callback)

previousSecond = datetime.datetime.now().timestamp()

def anim():
    global previousSecond
    if datetime.datetime.now().timestamp() - previousSecond >= seconds and simulation:
        continueDeBruler(foret, voisinageMoore)
        drawForet(canvas, width, height, size, foret)
        previousSecond = datetime.datetime.now().timestamp()
    main.after(100, anim)
anim()

drawLines(canvas, width, height, size)


foret = [[0 for j in range(height)] for i in range(width)]
startForet(foret, trees)
drawForet(canvas, width, height, size, foret)

main.mainloop()